# Bookshop Plugin

## Development

```bash
$ cd <plugin directory>
$ composer install
$ yarn install
$ yarn watch
```

In a new terminal:
```bash
$ cd <plugin directory>/svelte-components
$ yarn install
$ yarn dev
```

## Bundle for production

```bash
$ cd <plugin directory>/svelte-components
$ yarn build
$ cd ..
$ yarn bundle
```