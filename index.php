<?php
/**
 * Plugin Name: Mountain and Prairie Bookshop
 * Plugin URI: https://mountainandprairie.com
 * Description: The Mountain and Prairie bookshop.
 * Version: 0.1.0
 * Author: Nikki Lamagna & Ed Roberson
 * Author URI: https://mountainandprairie.com
 * License URI: MIT
 * Text Domain: wildmind-bookshop
 * Domain Path: /languages
 */

/**
 * Verify we have Pods installed and activated
 */
add_action( 'plugins_loaded', function() {
    include_once ABSPATH . 'wp-admin/includes/plugin.php';
    if ( ! is_plugin_active( 'pods/init.php' ) ) {
        add_action( 'admin_notices', function() {
            echo '<div class="notice notice-error is-dismissible"><p>The Bookshop plugin requires the Pods plugin to be installed and activated</p></div>';
        } );
    }
} );

require_once 'vendor/autoload.php';
require_once 'lib/utils.php';

new WildMind\Bookshop;
