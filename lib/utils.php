<?php

spl_autoload_register( function( $class_name ) {
    if ( false === strpos( $class_name, 'WildMind' ) ) {
        return;
    }

    $class_name = str_replace('\\', DIRECTORY_SEPARATOR, $class_name );
    $path = plugin_dir_path( __FILE__ );
    $path = $path . $class_name . '.php';
    require_once $path;
} );

function dd( $content, $var_dump = false, $die = true ) {
    echo '<pre>';
    $var_dump ? var_dump( $content ) : print_r( $content );
    echo '</pre>';
    if ( $die ) {
        die;
    }
}

function pluginPath() {
    $path = plugin_dir_path( dirname( __FILE__ ) );
    return $path;
}

function pluginURL() {
    $url = plugin_dir_url( dirname( __FILE__ ) );
    return $url;
}

function writeError( $error_string = '' ) {
    $file = pluginPath() . '/errors.txt';
    $open = fopen( $file, "a" );

    $date_time = new DateTime;
    $text = '[' . $date_time->format( 'Y-m-d G:i:s' ) . ']: ' . $error_string . "\n";
    fputs( $open, $text );
    fclose( $open );
    error_log( $text );
}
