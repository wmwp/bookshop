<?php

namespace WildMind;

use WP_Background_Process;
use WildMind\Book as Book;
use WildMind\ISBNdb as ISBNdb;

class SyncIsbnDBBackgroundProcess extends WP_Background_Process
{
    protected $action = 'wm_bd_import_isbndb_books';

    protected function task( $book_id )
    {
        if ( $book_id ) {
            $book = new Book( (int) $book_id );
            $book->syncWithIsbnDB();
        }
        return false;
    }

    protected function complete()
    {
        parent::complete();
        Notice::add( "Bookshop Alert: The ISBNdb sync has completed. Hopefully that saved you some work, eh? Keep on rocking the day!" );
    }
}
