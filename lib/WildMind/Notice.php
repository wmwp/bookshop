<?php

namespace WildMind;

class Notice
{
    /**
     * Types can be 'error', 'warning', 'success', 'info'
     */
    public static function add( String $message, $type = 'success', $nag = true, $screen = 'all' )
    {
        $notice = [
            'message' => $message,
            'nag' => $nag,
            'screen' => $screen,
            'time' => time(),
            'type' => $type
        ];
        $notices = get_option( 'wildmind_bd_notices' );
        if ( $notices ) {
            array_push( $notices, $notice );
        } else {
            $notices = [ $notice ];
        }
        $update = update_option( 'wildmind_bd_notices', $notices );
    }


    public static function display( $delete_notices = true )
    {
        global $current_screen;
        $notices = get_option( 'wildmind_bd_notices' );
        // dd($notices, false, false);
        if ( $notices ) {
            foreach ( $notices as $notice ) {
                if ( $notice['screen'] === 'all' || $current_screen->base === $notice['screen'] ) {
                    $message = htmlspecialchars( urldecode( $notice['message'] ) );
                    echo <<<EOT
<div class="notice notice-{$notice['type']} is-dismissible">
    <p>{$message}</p>
</div>
EOT;
                }
            }

            delete_option( 'wildmind_bd_notices' );
        }
    }
}
