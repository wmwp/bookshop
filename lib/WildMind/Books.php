<?php

namespace WildMind;
use WildMind\Author as Author;
use WildMind\Book as Book;
use Exception;

class Books {

    public function __construct()
    {
    }

    public static function getBooks( $query )
    {
        global $wpdb;
        $search = $query->get_query_params();
        $search_array = [];

        $limit = $search['limit'] ?? 36;
        $loaded_books = $search['loaded_books'];
        $page = $search['page'] ?? 1;
        $text = $search['text'] ?? '';
        $subject_id = $search['subject'] ?? 0;

        $return_data = [
            'total' => 0,
            'page' => $page,
            'limit' => $limit,
            'remaining' => 0,
            'results' => []
        ];

        $params = [
            'page' => sanitize_text_field( $page ),
            'pagination' => true,
            'limit' => sanitize_text_field( $limit )
        ];

        if ( $text ) {
            $text = $wpdb->prepare(
                '(t.post_title LIKE "%%%s%%" OR wm_bd_book_author.post_title LIKE "%%%s%%")',
                $text,
                $text
            );
            array_push( $search_array, $text );
        }

        if ( $subject_id > 0 ) {
            $subject_id = sanitize_text_field( $subject_id );
            array_push( $search_array, 'wm_bd_subjects.term_id = ' . $subject_id );
        }

        $book_ids = [];
        if ( ! empty( $loaded_books ) ) {
            array_push( $search_array, $wpdb->prepare(
                '(t.ID NOT IN (' . $loaded_books . '))'
            ) );
            $book_ids = explode( ',', $loaded_books );
        }

        if ( ! empty( $search_array ) ) {
            $params['where'] = implode( ' AND ', $search_array );
        } else {
            if ( $page === 1 ) {
                $params['where'] = $wpdb->prepare( '(wm_bd_book_author.post_title = "Jordan B. Peterson")' );
            }
            $params['orderby'] = 'RAND()';
        }
        $params['orderby'] = 'RAND()';

        $books = pods( 'wm_bd_books', $params, true );

        if ( $books === false ) {
            return [];
        } else {
            if ( $books->total() > 0 ) {
                $return_data['total'] = $books->total_found();
                $remaining = $books->total_found() - ( $page * $limit );
                $return_data['remaining'] = $remaining > 0 ? $remaining : 0;
                
                while( $books->fetch() ) {
                    $cover = $books->display( 'post_thumbnail_url.full' );
                    if ( ! $cover ) {
                        $cover = pluginURL() . 'assets/images/mandp-default-book-cover.png';
                    }

                    array_push( $book_ids, $books->display( 'ID' ) );
                    array_push( $return_data['results'], [
                        'authors' => $books->display( 'wm_bd_book_author.post_title' ),
                        'cover' => $cover,
                        'title' => $books->display( 'title' ),
                        'isbn' => $books->display( 'wm_bd_isbn' ),
                        'isbn13' => $books->display( 'wm_bd_isbn13' ),
                        'subjects' => $books->field( 'wm_bd_subjects' ),
                        'amazon_affiliate_link' => $books->field( 'wm_bd_amazon_affiliate_link' ),
                        'bookshop_affiliate_link' => $books->field( 'wm_bd_bookshop_affiliate_link' )
                    ] );
                }
            }
            $return_data['retrieved_book_ids'] = $book_ids;
            return $return_data;
        }
    }

    public static function getSubjects()
    {
        $return_data = [];
        $params = [
            'limit' => -1,
            'hide_empty' => true
        ];
        $subjects = get_terms( 'wm_bd_subjects', $params );
        if ( $subjects ) {
            foreach ( $subjects as $subject ) {
                array_push( $return_data, [
                    'id' => $subject->term_id,
                    'name' => $subject->name
                ] );
            }
        }
        return $return_data;
    }
}
