<?php

namespace WildMind;

use WP_Background_Process;
use WildMind\Book as Book;
use WildMind\ISBNdb as ISBNdb;
use WildMind\Notice as Notice;

class ImportCsvBackgroundProcess extends WP_Background_Process
{
    protected $action = 'wm_bd_import_csv';

    protected function task( $line )
    {
        if ( ! empty( $line[0] ) ) {
            $authors = trim( $line[1] );
            $authors = mb_convert_encoding( $authors, 'HTML-ENTITIES', 'UTF-8' );

            if ( strpos( $authors, ' and ' ) ) {
                $explode = explode( ' and ', $authors );
                $remaining = explode( ',', $explode[0] );
                unset( $explode[0] );
                $authors = [];
                foreach ( $remaining as $item ) {
                    if ( ! empty( $item ) ) {
                        array_push( $authors, trim( html_entity_decode( $item ) ) );
                    }
                }
                $authors = array_merge( $authors, $explode );
            } else {
                $authors = [ trim( $authors ) ];
            }


            $book = new Book;
            $title = mb_convert_encoding( $line[0], "UTF-8" );
            $title = html_entity_decode( $title, ENT_QUOTES | ENT_HTML5, "UTF-8" );
            $title = str_replace( '’', "'", $title );
            $book->setTitle( $title );
            $book->setAuthors( $authors );
            $book->setSubjects( explode( ',', mb_convert_encoding( html_entity_decode( $line[2] ), 'HTML-ENTITIES', 'UTF-8' ) ) );
            $book->setLastIsbnDbUpdate(0);
            $id = $book->save();

            if ( is_wp_error( $id ) ) {
                error_log( json_encode( $id ) );
            }
            return false;
        }
    }

    protected function complete()
    {
        parent::complete();
        Notice::add( "Bookshop Alert: The CSV import has completed. Aren't you industrious? Have a superb day!" );
    }
}
