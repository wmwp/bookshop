<?php

namespace WildMind;

class Author {
    private $books;
    private $id;
    private $name;

    public function __construct( String $name = null )
    {
        if ( $name ) {
            $this->name = trim( $name );
        }
    }

    public function get()
    {
        global $wpdb;
        $id = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s",
                $this->name,
                'wm_bd_authors'
            )
        );
        if ( ! $id ) {
            return false;
        }
        return $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function save()
    {
        $args = [
            'post_type' => 'wm_bd_authors',
            'post_title' => $this->name,
            'post_status' => 'publish',
            'comment_status' => 'closed',
            'ping_status' => 'closed'
        ];

        $id = $this->get();

        if ( $id ) {
            $args['ID'] = $id;
            wp_update_post( $args );
        } else {
            $id = wp_insert_post( $args );
        }

        if ( is_wp_error( $id ) ) {
            dd( $id );
        }
        $this->id = $id;
        return $this->id;
    }

    public function setID( Int $id )
    {
        $this->id = $id;
    }

    public function __toString()
    {
        return $this->name;
    }
}
