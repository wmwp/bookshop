<?php

namespace WildMind;

class Curl
{
    private $headers = [];

    public function setHeaders( Array $headers )
    {
        $this->headers = $headers;
    }

    public function get( String $url, $params = null )
    {
		if ( $params ) {
			$count = count( $params );
			$i = 1;
			$param = '';
			foreach ( $params as $key => $value ) {
				$value = preg_replace( '/ /', '+', $value );
				$param .= $key.'='.$value;
				if( $count > 1 && $i != $count ) {
					$param .= '&';
				}
				$i++;
			}
			$url = $url.'?'.$param;
		}

		$rest = $this->open();
		curl_setopt( $rest, CURLOPT_URL, $url );
		$contents = curl_exec( $rest );
		$this->close( $rest );
		return $contents;
	}

    private function open()
    {
		$rest = curl_init();
		curl_setopt( $rest, CURLOPT_HTTPHEADER, $this->headers );
		curl_setopt($rest, CURLOPT_RETURNTRANSFER, true);
		return $rest;
	}

    private function close( $rest )
    {
		return curl_close( $rest ) ? true : false;
	}

}
