<?php

namespace WildMind;

class Bookshop
{
    private $author_cpt = 'wm_bd_authors';
    private $bg_process;
    private $book_cpt = 'wm_bd_books';

    public function __construct()
    {
        /**
         * Filters
         */
        add_filter( 'enter_title_here', [ $this, 'changeAuthorTitleText' ] );
        add_filter( 'post_row_actions', [ $this, 'addSyncLinkToPostRowActions' ], 10, 2 );

        /**
         * Actions
         */
        add_action( 'admin_action_wmbdbookshop_csv', [ $this, 'handleCsvUpload' ] );
        add_action( 'admin_action_wmbdbookshop_settings', [ $this, 'updateSettings' ] );
        add_action( 'admin_action_wmbdbookshop_isbndb_sync_one', [ $this, 'syncOne' ] );
        add_action( 'admin_action_wmbdbookshop_sync', [ $this, 'sync' ] );
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueueAdmin' ] );
        add_action( 'admin_menu', [ $this, 'setupAdminPages' ] );
        add_action( 'admin_notices', [ 'WildMind\Notice', 'display' ] );
        add_action( 'init', [ $this, 'setup' ] );
        add_action( 'rest_api_init', [ $this, 'registerRestRoutes' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'enqueuePublic' ] );

        /**
         * Shortcodes
         */
        add_shortcode( 'mandp_bookshop', [ $this, 'shortCode' ] );

        /**
         * Background processes
         */
        $this->csv_bg = new ImportCsvBackgroundProcess();
        $this->sync_bg = new SyncIsbnDBBackgroundProcess();
    }

    public function addSyncLinkToPostRowActions( $actions, $post )
    {
        if ( $post->post_type === $this->book_cpt ) {
            $url = admin_url( 'post.php?post=' . $post->ID );
            $sync_url = wp_nonce_url( add_query_arg( [ 'action' => 'wmbdbookshop_isbndb_sync_one' ], $url ), 'sync_isbndb_nonce' );
            $actions = array_merge( $actions, [
                'sync' => sprintf( '<a href="%1$s">%2$s</a>', esc_url( $sync_url ), 'Sync with ISBNdb' )
            ] );
        }
        return $actions;
    }

    public function changeAuthorTitleText( $title )
    {
        $screen = get_current_screen();

        if ( 'wm_bd_authors' === $screen->post_type ) {
            $title = "Add author's first and last names";
        }
        return $title;
    }

    public function enqueueAdmin( $hook )
    {
        if ( $hook === 'toplevel_page_bookshop-admin' ) {
            $url = pluginURL();
            wp_enqueue_style( 'wm_bd_bookshop_styles', $url . 'dist/styles/style.css', [], '1.0.0' );
            wp_enqueue_script( 'wm_bd_bookshop_scripts', $url . 'dist/scripts/index.js', [], '1.0.0' );
        }
    }

    public function enqueuePublic( $hook )
    {
        $url = pluginURL();
        wp_enqueue_script( 'wm_bd_bookshop_svelte', $url . 'dist/svelte-bundle.js', [], '1.0.0', true );
        wp_enqueue_style( 'wm_bd_bookshop_svelte_styles', $url . 'dist/bundle.css', [], '1.0.0' );
    }

    public function getAffiliateIds()
    {
        $amazon_id = get_option( 'wm_bd_amazon' );
        $bookshop_id = get_option( 'wm_bd_bookshop' );
        return [
            'amazon_affiliate_id' => $amazon_id,
            'bookshop_affiliate_id' => $bookshop_id,
        ];
    }

    public function handleCsvUpload()
    {
        $message = [
            'message' => "CSV import has been kicked-off! Hooray! We'll let you know when the process is complete, with an alert like this. Have a swell day!",
            'type' => 'success'
        ];

        $file = wp_handle_upload(
            $_FILES['wm_bd_csv_file'],
            [
                'test_form' => false,
                'mimes' => [ 'csv' => 'text/csv' ]
            ]
        );

        if ( isset( $file['error'] ) ) {
            $message['message'] = $file['error'];
            $message['type'] = 'error';
        }

        if ( is_wp_error( $file ) ) {
            $message['message'] = $file['error'];
            $message['type'] = 'error';
        }

        $csv_file = fopen( $file['file'], 'r' );
        while ( !feof( $csv_file ) ) {
            $line = fgetcsv( $csv_file, 1024 );
            if ( ! empty( $line[0] ) ) {
                if ( $line[0] !== 'Title' ) {
                    $this->csv_bg->push_to_queue( $line );
                }
            }
        }
        $this->csv_bg->save()->dispatch();
        fclose( $csv_file );

        unlink( $file['file'] );

        return $message;
    }

    public function registerRestRoutes()
    {
        register_rest_route( 'bookshop/v1', '/books(?:/(?P<text>\s+))?', [
            'methods' => 'GET',
            'callback' => [ 'WildMind\Books', 'getBooks' ],
            'permission_callback' => '__return_true',
        ] );
        register_rest_route( 'bookshop/v1', '/subjects', [
            'methods' => 'GET',
            'callback' => [ 'WildMind\Books', 'getSubjects' ],
            'permission_callback' => '__return_true'
        ] );
        register_rest_route( 'bookshop/v1', '/affiliates', [
            'methods' => 'GET',
            'callback' => [ $this, 'getAffiliateIds' ],
            'permission_callback' => '__return_true'
        ] );
    }

    public function renderAdminPage()
    {
        $message = [];
        $path = pluginPath();

        if ( isset( $_POST['action'] ) && $_POST['action'] === 'wmbdbookshop_csv' ) {
            $message = $this->handleCsvUpload();
        }

        $api_key = get_option( 'wm_bd_isbndb_api_key', '' );
        $amazon = get_option( 'wm_bd_amazon', '' );
        $bookshop = get_option( 'wm_bd_bookshop', '' );

        include $path . '/pages/admin.php';
    }

    public function setup()
    {
        register_post_type( 'wm_bd_books', [
            'labels' => [
                'name' => __( 'Books', 'wildmind-bookshop' ),
                'singular_name' => __( 'Book', 'wildmind-bookshop' ),
                'add_new' => __( 'Add New', 'wildmind-bookshop' ),
                'add_new_item' => __( 'Add Book', 'wildmind-bookshop' ),
                'edit_item' => __( 'Edit Book', 'wildmind-bookshop' ),
                'new_item' => __( 'New Book', 'wildmind-bookshop' ),
                'view_item' => __( 'View Book', 'wildmind-bookshop' ),
                'view_items' => __( 'View Books', 'wildmind-bookshop' ),
                'search_items' => __( 'Search Books', 'wildmind-bookshop' ),
                'not_found' => __( 'No books found', 'wildmind-bookshop' ),
                'not_found_in_trash' => __( 'No books found in Trash', 'wildmind-bookshop' ),
                'all_items' => __( 'All Books', 'wildmind-bookshop' ),
                'archives' => __( 'Book Archives', 'wildmind-bookshop' ),
                'attributes' => __( 'Book Attributes', 'wildmind-bookshop' ),
                'insert_into_item' => __( 'Insert into book', 'wildmind-bookshop' ),
                'uploaded_to_this_item' => __( 'Uploaded to this book', 'wildmind-bookshop' ),
                'featured_image' => __( 'Book cover', 'wildmind-bookshop' ),
                'set_featured_image' => __( 'Set book cover', 'wildmind-bookshop' ),
                'remove_featured_image' => __( 'Remove book cover', 'wildmind-bookshop' ),
                'use_featured_image' => __( 'Use as book cover', 'wildmind-bookshop' ),
            ],
            'has_archive' => true,
            'menu_icon' => 'dashicons-book',
            'public' => true,
            'rewrite' => [ 'slug' => 'wm-bookshop-archive' ],
            'show_in_rest' => true,
            'supports' => [ 'thumbnail', 'post-formats', 'editor', 'title' ],
        ]);
        register_post_type( 'wm_bd_authors', [
            'labels' => [
                'name' => __( 'Authors', 'wildmind-bookshop' ),
                'singular_name' => __( 'Author', 'wildmind-bookshop' ),
                'add_new' => __( 'Add New', 'wildmind-bookshop' ),
                'add_new_item' => __( 'Add Author', 'wildmind-bookshop' ),
                'edit_item' => __( 'Edit Author', 'wildmind-bookshop' ),
                'new_item' => __( 'New Author', 'wildmind-bookshop' ),
                'view_item' => __( 'View Author', 'wildmind-bookshop' ),
                'view_items' => __( 'View Authors', 'wildmind-bookshop' ),
                'search_items' => __( 'Search Authors', 'wildmind-bookshop' ),
                'not_found' => __( 'No authors found', 'wildmind-bookshop' ),
                'not_found_in_trash' => __( 'No authors found in Trash', 'wildmind-bookshop' ),
                'all_items' => __( 'All Authors', 'wildmind-bookshop' ),
                'archives' => __( 'Author Archives', 'wildmind-bookshop' ),
                'attributes' => __( 'Author Attributes', 'wildmind-bookshop' ),
                'insert_into_item' => __( 'Insert into author', 'wildmind-bookshop' ),
                'uploaded_to_this_item' => __( 'Uploaded to this author', 'wildmind-bookshop' ),
                'featured_image' => __( 'Author Picture', 'wildmind-bookshop' ),
                'set_featured_image' => __( 'Set author picture', 'wildmind-bookshop' ),
                'remove_featured_image' => __( 'Remove author picture', 'wildmind-bookshop' ),
                'use_featured_image' => __( 'Use as author picture', 'wildmind-bookshop' ),
            ],
            'has_archive' => true,
            'menu_icon' => 'dashicons-admin-users',
            'public' => true,
            'show_in_rest' => true,
            'supports' => [ 'thumbnail', 'post-formats', 'editor', 'title' ]
        ]);

        register_taxonomy( 'wm_bd_subjects', 'wm_bd_books', [
            'labels' => [
                'name' => 'Book Subjects',
                'menu_name' => 'Subjects'
            ],
            'hierarchical' => true,
            'show_admin_column' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'query_var' => true
        ] );
        register_taxonomy_for_object_type( 'wm_bd_subjects', 'wm_bd_books' );
    }

    public function setupAdminPages()
    {
        add_menu_page(
            'Bookshop Administration',
            'Bookshop',
            'manage_options',
            'bookshop-admin',
            [ $this, 'renderAdminPage' ],
            'dashicons-book-alt'
        );
    }

    public function shortCode( $atts, $content, $tag )
    {
        return '<div id="wm-bookshop-app"></div>';
    }

    public function sync()
    {
        if ( isset( $_POST['action'] ) && $_POST['action'] === 'wmbdbookshop_sync' ) {
            $books = pods( 'wm_bd_books', [
		    'where' => 'wm_bd_last_isbndb_update.meta_value = 0',
		    'limit' => 1000
            ] );

            if ( $books->total() > 0 ) {
                while ( $books->fetch() ) {
                    $id = $books->display( 'ID' );
                    $queue = $this->sync_bg->push_to_queue( $id );
                }
                $this->sync_bg->save()->dispatch();
                $message = "ISBNdb sync has been kicked-off! Hooray! We will let you know when the process is complete, with an alert like this. Have an even better day!";
            } else {
                $message = "Well, look at you, being so on top of things! There are no books that have not been synced. Hooray! Have an even better day!";
            }

            wp_safe_redirect( 'admin.php?page=bookshop-admin&type=success&status_message=' . urlencode( $message ) );
        }
    }

    public function syncOne()
    {
        if ( wp_verify_nonce( $_GET['_wpnonce'], 'sync_isbndb_nonce' ) ) {
            $book = new Book( (int) $_GET['post'] );
            $book->syncWithIsbnDB();
        }
        exit( wp_redirect( admin_url( 'post.php?post=' . $_GET['post'] . '&action=edit' ) ) );
    }

    public function updateSettings()
    {
        $message = "Settings updated!";
        if ( isset( $_POST['action'] ) && $_POST['action'] === 'wmbdbookshop_settings' ) {
            if ( isset( $_POST['wm_bd_isbndb_api_key'] ) ) {
                $key = get_option( 'wm_bd_isbndb_api_key', '' );
                $updated_key = $_POST['wm_bd_isbndb_api_key'];
                if ( $key !== $updated_key ) {
                    update_option( 'wm_bd_isbndb_api_key', $_POST['wm_bd_isbndb_api_key'] );
                    $isbndb = new ISBNdb;
                    $isbndb->setIsbn('9780134093413');
                    $results = $isbndb->search();
                    if ( isset( $results->message ) ) {
                        $message = 'ISBNdb said <strong><em>' . $results->message . '</em></strong>';
                    }
                }
            }
            if ( isset( $_POST['wm_bd_amazon'] ) ) {
                update_option( 'wm_bd_amazon', $_POST['wm_bd_amazon'] );
            }
            if ( isset( $_POST['wm_bd_bookshop'] ) ) {
                update_option( 'wm_bd_bookshop', $_POST['wm_bd_bookshop'] );
            }

            wp_safe_redirect( 'admin.php?page=bookshop-admin&type=success&status_message=' . urlencode( $message ) );
        }
    }
}
