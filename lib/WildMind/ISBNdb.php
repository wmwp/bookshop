<?php

namespace WildMind;

use WildMind\Curl;

class ISBNdb
{
    private $api_key;
    private $authors = '';
    private $headers = [
        "Content-Type: application/json",
    ];
    private $isbn = '';
    private $isbn13 = '';
    private $publisher = '';
    private $root = 'https://api2.isbndb.com';
    private $subject = '';
    private $title = '';

    public function __construct()
    {
        $this->api_key = get_option( 'wm_bd_isbndb_api_key', '' );
        array_push( $this->headers, "Authorization: " . $this->api_key );
        $this->curl = new Curl;
        $this->curl->setHeaders( $this->headers );
    }

    public function search( String $type = 'books' )
    {
        $criteria = [];
        if ( ! empty( $this->authors ) ) {
            array_push( $criteria, 'author=' . rawurlencode( $this->authors ) );
        }
        if ( ! empty( $this->isbn ) ) {
            array_push( $criteria, 'isbn=' . rawurlencode( $this->isbn ) );
        }
        if ( ! empty( $this->isbn13 ) ) {
            array_push( $criteria, 'isbn13=' . rawurlencode( $this->isbn13 ) );
        }
        if ( ! empty( $this->publisher ) ) {
            array_push( $criteria, 'publisher=' . rawurlencode( $this->publisher ) );
        }
        if ( ! empty( $this->subject ) ) {
            array_push( $criteria, 'subject=' . rawurlencode( $this->subject ) );
        }
        if ( ! empty( $this->title ) ) {
            array_push( $criteria, 'text=' . rawurlencode( '"' . $this->title . '"' ) );
        }
        $criteria = join( '&', $criteria );
        $url = $this->root . '/search/' . $type . '?' . $criteria;
        $response = $this->curl->get( $url );
        return json_decode( $response );
    }

    public function searchByAuthors( String $authors )
    {
        $url = $this->root . '/search/books?author=';
        $authors = rawurlencode( $authors );
        $url .= $authors;

        $curl = new Curl;
        $curl->setHeaders( $this->headers );
        $response = $curl->get( $url );
        return json_decode( $response );
    }

    public function searchByTitle( String $title )
    {
        $url = $this->root . '/books/';
        $title = rawurlencode( $title );
        $url .= $title;

        $curl = new Curl;
        $curl->setHeaders( $this->headers );
        $response = $curl->get( $url );
        return json_decode( $response );
    }

    public function setAuthors( String $authors )
    {
        $this->authors = $authors;
    }

    public function setIsbn( String $isbn )
    {
        if ( strlen( $isbn ) === 10 ) {
            $this->isbn = $isbn;
        }
        if ( strlen( $isbn ) === 13 ) {
            $this->isbn13 = $isbn;
        }
    }

    public function setPublisher( String $publisher )
    {
        $this->publisher = $publisher;
    }

    public function setSubject( String $subject )
    {
        $this->subject = $subject;
    }

    public function setTitle( String $title )
    {
        $this->title = $title;
    }
}
