<?php

namespace WildMind;

use WildMind\Author as Author;
use WildMind\ISBNdb as ISBNdb;

class Book {
    private $authors = [];
    private $cover_url;
    private $date_published;
    private $format;
    private $id = 0;
    private $isbn;
    private $isbn13;
    private $language;
    private $last_isbndb_update = 0;
    private $msrp;
    private $pages;
    private $publisher;
    private $subjects = [];
    private $synopsis = '';
    private $title;

    public function __construct( Int $book_id = 0 )
    {
        if ( $book_id > 0 ) {
            $this->id = $book_id;
            $this->populateFromDb();
        }
    }

    public function createFromIsbnDB( \stdClass $obj )
    {
        if ( isset( $obj->authors ) ) {
            foreach ( $obj->authors as $author ) {
                $name = explode( ',', $author );
                foreach ( $name as &$n ) {
                    trim($n);
                }

                if ( isset( $name[1] ) ) {
                    $author_name = trim( $name[1] . ' ' . $name[0] );
                } else {
                    $author_name = $name[0];
                }

                foreach ( $this->authors as $attached_author ) {
                    if ( $attached_author->getName() !== $author_name ) {
                        $author = new Author( $author_name );
                        array_push( $this->authors, $author );
                    }
                }
            }
        }
        if ( isset( $obj->date_published ) ) {
            $this->date_published = (int) $obj->date_published;
        }
        if ( isset( $obj->format ) ) {
            $this->format = $obj->format;
        }
        if ( isset( $obj->image ) ) {
            $this->cover_url = $obj->image;
        }
        if ( isset( $obj->isbn ) ) {
            $this->isbn = $obj->isbn;
        }
        if ( isset( $obj->isbn13 ) ) {
            $this->isbn13 = $obj->isbn13;
        }
        if ( isset( $obj->language ) ) {
            $this->language = $obj->language;
        }
        if ( isset( $obj->pages ) ) {
            $this->pages = (int) $obj->pages;
        }
        if ( isset( $obj->publisher ) ) {
            $this->publisher = $obj->publisher;
        }
        if ( isset( $obj->subjects ) ) {
            $this->subjects = $obj->subjects;
        }
        if ( isset( $obj->synopsys ) ) {
            $this->synopsis = $obj->synopsys;
        }
        if ( isset( $obj->title ) ) {
            $this->title = $obj->title;
        }
    }

    public function get()
    {
        global $wpdb;
        $id = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s",
                $this->title,
                'wm_bd_books'
            )
        );
        if ( ! $id ) {
            return false;
        }
        return $id;
    }

    public function getAuthors( String $type = 'objects' ) {
        if ( $type === 'names' ) {
            $names = [];
            foreach ( $this->authors as $author ) {
                array_push( $names, $author );
            }
            return implode( ', ', $names );
        }
        return $this->authors;
    }

    public function getSubjects(): Array {
        return $this->subjects;
    }

    public function getTitle(): String {
        return $this->title;
    }

    public function save()
    {
        $args = [
            'post_type' => 'wm_bd_books',
            'post_title' => $this->title,
            'post_content' => $this->synopsis,
            'post_status' => 'publish',
            'comment_status' => 'closed',
            'ping_status' => 'closed'
        ];

        if ( $this->id > 0 ) {
            $args['ID'] = $this->id;
            $id = $this->id;
            wp_update_post( $args );
        } elseif ( $id = $this->get() ) {
            $args['ID'] = $id;
            wp_update_post( $args );
        } else {
            $id = wp_insert_post( $args );
        }

        if ( ! empty( $this->cover_url ) ) {
            $attachment_id = $this->insertBookCover( $id );
            if ( $attachment_id ) {
                set_post_thumbnail( $id, $attachment_id );
                $this->cover_url = wp_get_attachment_url( $attachment_id );
            }
        }

        if ( is_wp_error( $id ) ) {
		    error_log( 'Could not save book cover to book id ' . $id );
        }
        $this->id = $id;

        // Subjects
        if ( ! empty( $this->subjects ) ) {
            $subjects = [];
            foreach ( $this->subjects as $subject ) {
                if ( ! empty( $subject ) ) {
                    $subject_id = wp_insert_term( $subject, 'wm_bd_subjects' );
                    if ( is_wp_error( $subject_id ) ) {
                        $subject_id = $subject_id->get_error_data(); // this returns the ID
                    }
                    array_push( $subjects, $subject_id );
                }
            }

            if ( ! empty( $subjects ) ) {
                wp_set_post_terms( $this->id, $subjects, 'wm_bd_subjects', true );
            }
        }

        // Authors
        if ( ! empty( $this->authors ) ) {
            $authors = [];
            foreach ( $this->authors as $author ) {
                $author_id = $author->save();
                array_push( $authors, $author_id );
            }
        }

        // Let's add the metadata
        $pod = pods( 'wm_bd_books', $id );
        $meta = [
            'wm_bd_book_author' => $authors
        ];
        if ( ! empty( $this->date_published ) ) {
            $meta['wm_bd_date_published'] = $this->date_published;
        }
        if ( ! empty( $this->format ) ) {
            $meta['wm_bd_format'] = $this->format;
        }
        if ( ! empty( $this->isbn ) ) {
            $meta['wm_bd_isbn'] = $this->isbn;
        }
        if ( ! empty( $this->isbn13 ) ) {
            $meta['wm_bd_isbn13'] = $this->isbn13;
        }
        if ( ! empty( $this->language ) ) {
            $meta['wm_bd_language'] = $this->language;
        }
        if ( ! empty( $this->pages ) ) {
            $meta['wm_bd_pages'] = $this->pages;
        }
        if ( ! empty( $this->publisher ) ) {
            $meta['wm_bd_publisher'] = $this->publisher;
        }
        if ( ! empty( $this->last_isbndb_update ) || $this->last_isbndb_update === 0 ) {
            $meta['wm_bd_last_isbndb_update'] = $this->last_isbndb_update;
        }

        $pod->save( $meta );

        return $this->id;
    }

    public function setAuthors( Array $authors )
    {
        $names = [];
        foreach ( $authors as $name ) {
            array_push( $names, new Author( $name ) );
        }
        $this->authors = $names;
    }

    public function setLastIsbnDbUpdate( $time = 0 )
    {
        $this->last_isbndb_update = $time;
    }

    public function setSubjects( Array $subjects )
    {
        $this->subjects = $subjects;
    }

    public function setTitle( String $title )
    {
        $this->title = $title;
    }

    public function syncWithIsbnDB()
    {
        $isbndb = new ISBNdb;

        if ( ! empty( $this->getTitle() ) ) {
            $this_book = [];
            $isbndb->setTitle( $this->getTitle() );
            $authors = $this->getAuthors();
            $authors = array_map( function ( $a ) {
                return str_replace( ' ', '', strtolower( $a->getName() ) );
            }, $authors );

            $results = $isbndb->searchByTitle( $this->getTitle() );

            if ( isset( $results->errorMessage ) ) {
                writeError( '(syncWithIsbnDB) ' . $this->getTitle() . ': ' . $results->errorMessage );
                return false;
            }

            if ( $results && isset( $results->books ) ) {
                $books = $results->books;
                usort( $books, function( $a, $b ) {
                    return $a->date_published > $b->date_published;
                } );

                foreach ( $books as $result ) {

                    /**
                     * We need to make sure the author matches before continuing
                     */
                    $author_match = false;

                    // If the ISBNdb results has authors
                    if ( isset( $result->authors ) ) {
                        // The authors is an array
                        foreach ( $result->authors as $index => $author ) {
                            // ISBNdb returns last_name, first_name so need to massage the data
                            // Take the authors and convert to a string, without spaces, all
                            // lowercase so that we can compare with what's in our db
                            $result_author = explode( ',', $author );
                            $result_author = array_map( function ( $a ) {
                                return trim( strtolower( str_replace( ' ', '', $a ) ) );
                            }, $result_author );
                            $result_author = array_reverse( $result_author );
                            $result_author = implode( '', $result_author );

                            if ( in_array( $result_author, $authors ) ) {
                                $author_match = true;
                                break;
                            }

                            // If this didn't match, let's just check for last name
                            if ( strpos( $author, ',' ) > -1 ) {
                                $result_author = explode( ',', $author );
                                if ( in_array( $result_author[0], $authors ) ) {
                                    $author_match = true;
                                    break;
                                }
                            } else {
                                $result_author = explode( ' ', $author );
                                $result_author = array_reverse( $result_author );
                                foreach ( $authors as $a ) {
                                    $last_name = strtolower( $result_author[0] );
                                    if ( strpos( $a, $last_name ) > -1 ) {
                                        $author_match = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (
                        $author_match
                        && isset( $result->binding )
                        && ( strtolower( $result->binding ) === 'paperback' || strtolower( $result->binding ) === 'hardcover' )
                    ) {
                        if (
                            isset( $result->language )
                            && ( strtolower( $result->language ) !== 'en' && strtolower( $result->language ) !== 'en_us' )
                        ) {
                            continue;
                        } else {
                            // We need to find the latest edition
                            $this_book[ $result->date_published ] = $result;
                        }
                    }
                }
            }

            $saved = false;
            /*
             * We're going to sort the matched books by date asc, so the oldest book is the first one.
             * Later editions are often translations or anniversary editions, which isn't ideal.
             */
            ksort( $this_book );

            foreach ( $this_book as $b ) {
                if ( isset( $b->synopsys ) ) {
                    $this->setLastIsbnDbUpdate( time() );
                    $this->createFromIsbnDB( $b );
                    $this->save();
                    $saved = true;
                    break;
                }
            }

            if ( ! $saved ) {
                if ( ! empty( $this_book ) ) {
                    $this->setLastIsbnDbUpdate( time() );
                    $this->createFromIsbnDB( array_shift( $this_book ) );
                    $this->save();
                } else {
                    writeError( '(syncWithIsbnDB) ' . $this->getTitle() . ': No match found' );
                }
            }
        }
        return $saved;
    }

    public function changeUploadDir( $arr )
    {
        $folder = '/cover-images';

        $basedir = $arr['basedir'];
        $baseurl = $arr['baseurl'];

        $arr['path'] = $basedir . $folder;
        $arr['url'] = $baseurl . $folder;
        $arr['subdir'] = $folder;

        return $arr;
    }

    public function insertBookCover( $id )
    {
        require_once ABSPATH . 'wp-admin/includes/image.php';
        require_once ABSPATH . 'wp-admin/includes/file.php';
        require_once ABSPATH . 'wp-admin/includes/media.php';

        $file['name'] = $this->cover_url;
        $file['tmp_name'] = download_url( $this->cover_url );

        if ( is_wp_error( $file['tmp_name'] ) ) {
            writeError( 'Could not save book cover to book id ' . $id . '. ERROR: ' . $file['tmp_name']->get_error_messages() );
        }

        add_filter( 'upload_dir', [ $this, 'changeUploadDir' ] );

        $attachment_id = media_handle_sideload( $file, $id, $this->getTitle() . ' Book Cover' );

        if ( is_wp_error( $attachment_id ) ) {
            writeError( 'WP Upload Error [book title: ' . $this->getTitle() . '] ' . json_encode( $attachment_id->get_error_messages() ) );
            return false;
        }

        remove_filter( 'upload_dir', [ $this, 'changeUploadDir' ] );
        return $attachment_id;
    }

    private function populateFromDb()
    {
        $book = pods( 'wm_bd_books', $this->id );
        if ( $book->exists() ) {
            $this->title = $book->field( 'title' );
            $authors = $book->field( [ 'name' => 'wm_bd_book_author' ] );
            $names = [];
            foreach ( $authors as $author ) {
                $a = new Author( $author['post_title'] );
                $a->setID( $author['ID'] );
                array_push( $names, $a );
            }
            $this->authors = $names;
        }
    }
}
