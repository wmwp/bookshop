<div class="wrap bookshop-admin">
    <h1 class="wp-heading-inline">Bookshop Admin</h1>

    <?php if ( isset( $message['message'] ) ) { ?>
    <div class="notice notice-<?php echo trim( $message['type'] ); ?> is-dismissible">
        <p><?php echo $message['message']; ?></p>
    </div>
    <?php } ?>

    <?php if ( isset( $_GET['status_message'] ) ) { ?>
    <div class="notice notice-<?php echo trim( urldecode( $_GET['type'] ) ); ?> is-dismissible">
        <p><?php echo urldecode( $_GET['status_message'] ); ?></p>
    </div>
    <?php } ?>

    <div class="main">
        <div>
            <h2>Settings</h2>
            <form id="wmbdbookshop_settings" method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th><label for="wm_bd_isbndb_api_key">ISBNdb API Key</label></th>
                            <td>
                                <input type="text" id="wm_bd_isbndb_api_key" name="wm_bd_isbndb_api_key" value="<?php echo $api_key; ?>" />
                                <p class="description">Get your ISBNdb API Key <a href="https://isbndb.com/isbn-database">here</a>.</p>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="wm_bd_bookshop">Bookshop Affiliation ID</label></th>
                            <td><input type="text" id="wm_bd_bookshop" name="wm_bd_bookshop" value="<?php echo $bookshop; ?>" /></td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" name="action" value="wmbdbookshop_settings" />
                <p><input type="submit" value="Update" class="button button-primary button-large" /></p>
            </form>
        </div>

        <div>
            <h2>CSV Import</h2>
            <p>Please upload a CSV in the following format:</p>
            <pre><code>"Title", "Author", "Categories"
"All the Light We Cannot See", "Anthony Doerr", "Fiction, Historical Fiction"</code></pre>

            <form id="csv-upload" method="POST" action="<?php echo admin_url( 'admin.php?page=bookshop-admin' ); ?>" enctype="multipart/form-data">
                <p>
                    <strong><label for="wm_bd_csv_file">CSV file</label></strong>
                    <input type="file" name="wm_bd_csv_file" />
                <p>
                <input type="hidden" name="action" value="wmbdbookshop_csv" />
                <p>
                    <input type="submit" value="Upload CSV file" class="button button-primary button-large" />
                </p>
            </form>
        </div>

        <div>
            <h2>ISBNdb Sync</h2>
            <p>Syncing with ISBNdb will update every book that hasn't been updated before. If you want to update single book titles, that is not available at this time. This process runs in the background and an alert will be displayed when it has been completed.</p>
            <form id="sync" method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
                <input type="hidden" name="action" value="wmbdbookshop_sync" />
                <input type="submit" value="Sync with ISBNdb" class="button button-primary button-large" />
            </form>
        </div>
    </div>

</div>
